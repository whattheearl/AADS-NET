﻿namespace Sorting
{
    public interface ISort<T>
    {
        T[] Sort();
    }
}