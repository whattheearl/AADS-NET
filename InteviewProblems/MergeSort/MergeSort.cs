﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorting
{
    public class MergeSort : ISort<int>
    {
        private int[] _collection { get; set; }

        public MergeSort(int[] a)
        {
            if (a != null)
            {
                _collection = a;
            }
        }

        public int[] Sort()
        {
            return sort(_collection, 0, _collection.Length-1);
        }

        private int[] sort(int[] a, int l, int r)
        {
            if(l < 0 || r < 0)
            {
                throw new Exception(string.Format("Counter l={0} or r={1} is a negative number", l, r));
            }

            int[] left = null;
            int[] right = null;
            if (l < r)
            {
                int mid = (l + r) / 2;
                left = sort(a, l, mid);
                right = sort(a, mid + 1, r);
            }
            else
            {
                left = new int[] { a[l] };
            }

            return merge(left, right);
        }

        private int[] merge(int[] left, int[] right)
        {
            // if any of the inputs is null then set it to be an empty array
            left = left ?? new int[0];
            right = right ?? new int[0];

            int[] result = new int[left.Length + right.Length];
            int result_counter = 0, left_counter = 0, right_counter = 0;

            int? tmp = 0;
            while (result_counter < result.Length)
            {
                if(left_counter < left.Length 
                    && (right_counter >= right.Length || left[left_counter] < right[right_counter]))
                {
                    tmp = left[left_counter++];
                }
                else if(right_counter < right.Length)
                {
                    tmp = right[right_counter++];
                }
                else
                {
                    throw new Exception(string.Format("counters went out of bount: left.Length={0}&&left_counter={1}, right.Length={2}&&right_counter={3}, result.Length={4}&&result_counter={5}", left.Length, left_counter, right.Length, right_counter, result.Length, result_counter));
                }

                if (tmp != null)
                {
                    result[result_counter++] = (int)tmp;
                }
            }

            return result;
        }
    }
}
