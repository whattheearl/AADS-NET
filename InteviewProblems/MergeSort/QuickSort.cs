﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorting
{
    public class QuickSort : ISort<int>
    {
        private int[] Collection { get; set; }
        public int swaps { get; set; }

        public QuickSort(int[] a)
        {
            if (a != null)
            {
                Collection = a;
            }
        }

        public int[] Sort()
        {
            sort(Collection, 0, Collection.Length-1);
            return Collection;
        }

        private void sort(int[] a, int l, int r)
        {
            int index = partition(a, l, r);
            if (l < index - 1)
            {
                sort(a,l,index-1);
            }

            if (index < r)
            {
                sort(a, index,r);
            }
        }

        private int partition(int[] a, int l, int r)
        {
            int pivot = a[(l + r) / 2];
            while (l <= r)
            {
                while (a[l] < pivot)
                {
                    l++;
                }

                while (a[r] > pivot)
                {
                    r--;
                }

                if(l <= r)
                {
                    swap(a, l, r);
                    l++;
                    r--;
                }
            }
            return l;
        }

        private void swap(int[] a, int l, int r)
        {
            swaps++;
            int tmp = a[l];
            a[l] = a[r];
            a[r] = tmp;
        }
    }
}
