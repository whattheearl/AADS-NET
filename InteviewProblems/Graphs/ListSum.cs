﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphs
{
    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int x) { val = x; }

        public override string ToString()
        {
            string result = string.Empty;
            var tmp = this;
            bool first = true;
            while(tmp != null)
            {
                if (!first)
                {
                    result += ",";
                }

                result += tmp.val;
                first = false;

                tmp = tmp.next;
            }

            return result;
        }
    }

    public class ListProblems
    {
        public ListNode AddTwoNumbers(ListNode l1, ListNode l2)
        {
            ListNode result = null;
            ListNode tmp = null;

            while (l1 != null || l2 != null)
            {
                int sum = 0;
                if (l1 != null)
                {
                    sum += l1.val;
                    l1 = l1.next;
                }

                if (l2 != null)
                {
                    sum += l2.val;
                    l2 = l2.next;
                }

                // this should only hit the first time
                if (tmp == null)
                {
                    tmp = new ListNode(sum % 10);
                    if(result==null) result = tmp;
                }
                else
                {
                    sum += tmp.val;
                    tmp.val = sum % 10;
                }

                if (l1 == null && l2 == null && sum/10 < 1)
                {

                }
                else
                {
                    tmp.next = new ListNode(sum / 10);
                }

                tmp = tmp.next;
            }

            return result;
        }
    }
}
