﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphs
{
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int x) { val = x; }
        public override string ToString()
        {
            return val.ToString();
        }
    }

    public class LCA
    {
        public TreeNode LowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q)
        {
            if (root == null || p == null || q == null) return null;

            // traverse to the very leaf
            TreeNode l = LowestCommonAncestor(root.left, p, q);
            TreeNode r = LowestCommonAncestor(root.right, p, q);

            // edgecase to catch when the root found is the same value as both p and q
            if(l != null && l.val == p.val && l.val == q.val)
            {
                return l;
            }
            if (r != null && r.val == p.val && r.val == q.val)
            {
                return r;
            }

            bool left_found = false;
            bool right_found = false;

            // if the first value (p) is this node, or if it is found in one of the subtrees
            if(p.val == root.val || (l!=null && p.val == l.val) || (r!=null && p.val == r.val))
            {
                left_found = true;
            }

            // if the second value (q) is this node, or if it is found in one of the subtrees
            if (q.val == root.val || (l != null && q.val == l.val) || (r != null && q.val == r.val))
            {
                right_found = true;
            }

            // if both values are found then this Node is the LCA
            if (left_found && right_found)
            {
                return root;
            }

            // See if we found one of the nodes
            if (root.val == p.val) return p;
            if (root.val == q.val) return q;

            // if the return value must be an LCA found in the subtree
            // just pass it back
            if (l != null)
            {
                return l;
            }
            if (r != null)
            {
                return r;
            }

            // reached leaf
            return null;
        }

        public TreeNode LowestCommonAncestor_2(TreeNode root, TreeNode p, TreeNode q)
        {
            // Base case
            if (root == null) return null;

            // If either p or q matches with root's key, report
            // the presence by returning root (Note that if a key is
            // ancestor of other, then the ancestor key becomes LCA
            if (root.val == p.val || root.val == q.val)
                return root;

            // Look for keys in left and right subtrees
            TreeNode left_lca = LowestCommonAncestor(root.left, p, q);
            TreeNode right_lca = LowestCommonAncestor(root.right, p, q);

            // If both of the above calls return Non-null, then one key
            // is present in once subtree and other is present in other,
            // So this node is the LCA
            if (left_lca != null && right_lca != null) return root;

            // Otherwise check if left subtree or right subtree is LCA
            return (left_lca != null) ? left_lca : right_lca;
        }

        //find the search node below root
        bool findNode(TreeNode root, TreeNode search)
        {
            //base case
            if (root == null)
                return false;

            if (root.val == search.val)
                return true;

            //search for the node in the left and right subtrees, if found in either return true
            return (findNode(root.left, search) || findNode(root.right, search));
        }

        //returns the LCA, p & q are the 2 nodes for which we are
        //establishing the LCA for
        public TreeNode LowestCommonAncestor_3(TreeNode root, TreeNode p, TreeNode q)
        {
            //base case
            if (root == null)
                return null;

            //If 1 of the nodes is the root then the root is the LCA
            //no need to recurse.
            if (p == root || q == root)
                return root;

            //check on which side of the root p and q reside
            bool n1OnLeft = findNode(root.left, p);
            bool n2OnLeft = findNode(root.left, q);

            //p & q are on different sides of the root, so root is the LCA
            if (n1OnLeft != n2OnLeft)
                return root;

            //if both p & q are on the left of the root traverse left sub tree only
            //to find the node where p & q diverge otherwise traverse right subtree
            if (n1OnLeft)
                return LowestCommonAncestor_3(root.left, p, q);
            else
                return LowestCommonAncestor_3(root.right, p, q);
        }

        // Finds the path from root node to given root of the tree, Stores the
        // path in a List path[], returns true if path exists otherwise false
        bool findPath(TreeNode root, ref List<TreeNode> path, int k)
        {
            // base case
            if (root == null) return false;

            // Store this node in path List. The node will be removed if
            // not in path from root to k
            path.Add(root);

            // See if the k is same as root's key
            if (root.val == k)
                return true;

            // Check if k is found in left or right sub-tree
            if ((root.left != null && findPath(root.left, ref path, k)) ||
                 (root.right != null && findPath(root.right, ref path, k)))
                return true;

            // If not present in subtree rooted with root, remove root from
            // path[] and return false
            path.RemoveAt(path.Count() - 1);
            return false;
        }

        // Returns LCA if node p, q are present in the given binary tree,
        // otherwise return -1
        public TreeNode LowestCommonAncestor_4(TreeNode root, TreeNode p, TreeNode q)
        {
            // to store paths to p and q from the root
            List<TreeNode> path1 = new List<TreeNode>();
            List<TreeNode> path2 = new List<TreeNode>();

            // Find paths from root to p and root to p. If either p or q
            // is not present, return -1
            if (!findPath(root, ref path1, p.val) || !findPath(root, ref path2, q.val))
                return null;

            /* Compare the paths to get the first different value */
            int i;
            for (i = 0; i < path1.Count() && i < path2.Count(); i++)
                if (path1[i] != path2[i])
                    break;
            return path1[i - 1];
        }
    }
}
