﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Graphs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphs.Tests
{
    [TestClass()]
    public class LCATests
    {
        Dictionary<int, TreeNode> nodes = new Dictionary<int, TreeNode>();

        [TestMethod()]
        public void LowestCommonAncestorTest()
        {
            LCA target = new LCA();

            /*
                    _______3______
                   /              \
                ___5__          ___1__
               /      \        /      \
              _6      _2       0       8
             /  \    /  \
           -48 -100 7   4
            */
            TreeNode root = helper_createSampleTree();

            // 5&1=3
            TreeNode p = nodes[5];
            TreeNode q = nodes[1];
            TreeNode expected = nodes[3];
            TreeNode actual = target.LowestCommonAncestor(root, p, q);
            Assert.AreEqual(expected.val, actual.val);

            // 5&2=5
            p = nodes[5];
            q = nodes[2];
            expected = nodes[5];
            actual = target.LowestCommonAncestor(root, p, q);
            Assert.AreEqual(expected.val, actual.val);

            // 5&6=5
            p = nodes[5];
            q = nodes[6];
            expected = nodes[5];
            actual = target.LowestCommonAncestor(root, p, q);
            Assert.AreEqual(expected.val, actual.val);

            // 5&4=5
            p = nodes[5];
            q = nodes[4];
            expected = nodes[5];
            actual = target.LowestCommonAncestor(root, p, q);
            Assert.AreEqual(expected.val, actual.val);

            // 5&7=5
            p = nodes[5];
            q = nodes[7];
            expected = nodes[5];
            actual = target.LowestCommonAncestor(root, p, q);
            Assert.AreEqual(expected.val, actual.val);

            // 5&5=5
            p = nodes[5];
            q = nodes[5];
            expected = nodes[5];
            actual = target.LowestCommonAncestor(root, p, q);
            Assert.AreEqual(expected.val, actual.val);

            // 1&1=1
            p = nodes[1];
            q = nodes[1];
            expected = nodes[1];
            actual = target.LowestCommonAncestor(root, p, q);
            Assert.AreEqual(expected.val, actual.val);

            // 6&2=5
            p = nodes[6];
            q = nodes[2];
            expected = nodes[5];
            actual = target.LowestCommonAncestor(root, p, q);
            Assert.AreEqual(expected.val, actual.val);

            // now make a random tree
            Dictionary<int, bool> unique = new Dictionary<int, bool>();
            nodes = new Dictionary<int, TreeNode>();
            root = null;
            var random = new Random();

            for (int i = 0; i < 100; i++)
            {
                var next = random.Next(-100, 100);
                try
                {
                    while (unique[next])
                    {
                        next = random.Next(-100, 100);
                    }
                }
                catch
                {
                    unique.Add(next, true);
                    TreeNode tmp = new TreeNode(next);
                    insert(ref root, tmp);
                    nodes.Add(tmp.val, tmp);
                }
            }

            /*using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"D:\LCA.log"))
            {
                file.WriteLine(To_String(root));
            }*/

            // and run the code for every combination possible
            foreach (var a in nodes.Values)
            {
                foreach (var b in nodes.Values)
                {
                    expected = target.LowestCommonAncestor_4(root, a, b);
                    actual = target.LowestCommonAncestor(root, a, b);
                    Assert.AreEqual(expected.val, actual.val);
                }
            }

            // add an exisitng value to make it non-unique
            insert(ref root, new TreeNode(root.val));

            // and run the code for every combination possible
            foreach (var a in nodes.Values)
            {
                foreach (var b in nodes.Values)
                {
                    expected = target.LowestCommonAncestor_4(root, a, b);
                    actual = target.LowestCommonAncestor(root, a, b);
                    Assert.AreEqual(expected.val, actual.val);
                }
            }
        }

        private TreeNode helper_createSampleTree()
        {
            TreeNode root = new TreeNode(3);
            TreeNode tmp = root;
            nodes.Add(tmp.val, tmp);

            tmp = new TreeNode(5);
            nodes.Add(tmp.val, tmp);
            nodes[3].left = tmp;

            tmp = new TreeNode(1);
            nodes.Add(tmp.val, tmp);
            nodes[3].right = tmp;

            tmp = new TreeNode(6);
            nodes.Add(tmp.val, tmp);
            nodes[5].left = tmp;

            tmp = new TreeNode(-48);
            nodes.Add(tmp.val, tmp);
            nodes[6].left = tmp;

            tmp = new TreeNode(-100);
            nodes.Add(tmp.val, tmp);
            nodes[6].right = tmp;

            tmp = new TreeNode(2);
            nodes.Add(tmp.val, tmp);
            nodes[5].right = tmp;

            tmp = new TreeNode(7);
            nodes.Add(tmp.val, tmp);
            nodes[2].left = tmp;

            tmp = new TreeNode(4);
            nodes.Add(tmp.val, tmp);
            nodes[2].right = tmp;

            tmp = new TreeNode(0);
            nodes.Add(tmp.val, tmp);
            nodes[1].left = tmp;

            tmp = new TreeNode(8);
            nodes.Add(tmp.val, tmp);
            nodes[1].right = tmp;

            return root;
        }

        public bool insert(ref TreeNode r, TreeNode n)
        {
            if (r == null)
            {
                r = n;
                return true;
            }

            if(r.left == null)
            {
                r.left = n;
                return true;
            }
            if(r.right == null)
            {
                r.right = n;
                return true;
            }

            if (!insert(ref r.left, n))
            {
                return insert(ref r.right, n);
            }
            else
            {
                return true;
            }

            return false;
        }

        public string To_String(TreeNode r)
        {
            if (r == null) return string.Empty;

            return To_String(r.left) + r.val + To_String(r.right);
        }
    }
}