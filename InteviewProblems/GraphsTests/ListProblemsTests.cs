﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Graphs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;
namespace Graphs.Tests
{
    [TestClass()]
    public class ListProblemsTests
    {
        [TestMethod()]
        public void AddTwoNumbersTest()
        {
            //[2,4,3]
            //[5,6,4]
            ListNode l1 = new ListNode(2);
            l1.next = new ListNode(4);
            l1.next.next = new ListNode(3);
            ListNode l2 = new ListNode(5);
            l2.next = new ListNode(6);
            l2.next.next = new ListNode(4);

            ListProblems target = new ListProblems();

            var expected = new ListNode(7);
            expected.next = new ListNode(0);
            expected.next.next = new ListNode(8);
            var actual = target.AddTwoNumbers(l1, l2);

            if (!TestUtilities.ListNodeListsAreEqual(expected, actual))
            {
                Assert.Fail("Lists are not equal: expected[{0}], actual[{1}]", expected.ToString(), actual.ToString());
            }
        }

        [TestMethod()]
        public void AddTwoNumbersTest_1()
        {
            //[5]
            //[5]
            ListNode l1 = new ListNode(5);
            ListNode l2 = new ListNode(5);

            ListProblems target = new ListProblems();

            var expected = new ListNode(0);
            expected.next = new ListNode(1);
            var actual = target.AddTwoNumbers(l1, l2);

            if (!TestUtilities.ListNodeListsAreEqual(expected, actual))
            {
                Assert.Fail("Lists are not equal: expected[{0}], actual[{1}]", expected.ToString(), actual.ToString());
            }
        }
    }
}