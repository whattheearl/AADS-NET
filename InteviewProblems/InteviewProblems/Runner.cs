﻿using Sorting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace InteviewProblems
{
    class Runner
    {
        static void Main(string[] args)
        {
            DateTime before;
            DateTime after;
            TimeSpan elapsed;
            List<TimeSpan> allelapsed = new List<TimeSpan>();
            int[] array= new int[] { 8, 5, 7, 9, 3, 2 };
            int[] expected= new int[] { 2, 3, 5, 7, 8, 9 };

            string currentTestName = "QuickSort";
            Console.WriteLine(string.Format("Test {0}", currentTestName));
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"D:\QuickSort.log"))
            {
                for (int i = 0; i < 1; i++)
                {
                    Console.WriteLine("Input:{0}",ArrayToString(array));
                    Console.WriteLine("Expected:{0}", ArrayToString(expected));
                    ISort<int> sort = new QuickSort(array);

                    before = DateTime.UtcNow;
                    int[] actual = sort.Sort();
                    after = DateTime.UtcNow;
                    elapsed = after - before;
                    allelapsed.Add(elapsed);
                    Console.WriteLine("Actual:{0}", ArrayToString(actual));
                    //Console.WriteLine("Swaps={0}", ((QuickSort)sort).swaps);
                }

                var average = allelapsed.Average(a => a.Ticks);
                string logline = string.Format("Average time {0} ticks", average);
                file.WriteLine(logline);
                Console.WriteLine(logline);
            }
            Console.Write("Press any key to continue...");
            Console.ReadKey();
        }

        public static string ArrayToString(IEnumerable<int> a)
        {
            string result = string.Empty;

            for (int i = 0; a != null && i < a.Count(); i++)
            {
                if (i > 0)
                {
                    result += ",";
                }
                result += a.ElementAtOrDefault(i);
            }

            return result;
        }
    }
}
