﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sorting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace Sorting.Tests
{
    [TestClass()]
    public class QuickSortTests
    {
        private int test_sample_size = 1000;
        ISort<int> sort;

        [TestMethod()]
        public void SortTest()
        {
            int[] array = TestUtilities.GetRandomArray(test_sample_size);
            int[] expected = array.OrderBy(a => a).ToArray();
            sort = new QuickSort(array);
            int[] actual = sort.Sort();
            if (!TestUtilities.ArraysAreEqual(expected, actual))
            {
                Assert.Fail(string.Format("arrays aren't equal expected:{0}, actual:{1}", TestUtilities.EnumerableOfIntToString(expected), TestUtilities.EnumerableOfIntToString(actual)));
            }
        }

        [TestMethod()]
        public void SortTest_negative()
        {
            int[] array = TestUtilities.GetRandomArray(test_sample_size, test_sample_size/-2, test_sample_size/2);
            int[] expected = array.OrderBy(a => a).ToArray();
            sort = new QuickSort(array);
            int[] actual = sort.Sort();
            if (!TestUtilities.ArraysAreEqual(expected, actual))
            {
                Assert.Fail(string.Format("arrays aren't equal expected:{0}, actual:{1}", TestUtilities.EnumerableOfIntToString(expected), TestUtilities.EnumerableOfIntToString(actual)));
            }
        }

        [TestMethod()]
        public void SortTest_oppoiste_sorted()
        {
            int[] array = TestUtilities.GetRandomArray(test_sample_size).OrderByDescending(a=>a).ToArray();
            int[] expected = array.OrderBy(a => a).ToArray();
            sort = new QuickSort(array);
            int[] actual = sort.Sort();
            if (!TestUtilities.ArraysAreEqual(expected, actual))
            {
                Assert.Fail(string.Format("arrays aren't equal expected:{0}, actual:{1}", TestUtilities.EnumerableOfIntToString(expected), TestUtilities.EnumerableOfIntToString(actual)));
            }
        }

        [TestMethod()]
        public void SortTest_sorted()
        {
            int[] array = TestUtilities.GetRandomArray(test_sample_size).OrderBy(a => a).ToArray();
            int[] expected = array.OrderBy(a => a).ToArray();
            sort = new QuickSort(array);
            int[] actual = sort.Sort();
            if (!TestUtilities.ArraysAreEqual(expected, actual))
            {
                Assert.Fail(string.Format("arrays aren't equal expected:{0}, actual:{1}", TestUtilities.EnumerableOfIntToString(expected), TestUtilities.EnumerableOfIntToString(actual)));
            }
        }
    }
}
