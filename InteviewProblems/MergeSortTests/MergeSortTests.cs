﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MergeSort;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MergeSort.Tests
{
    [TestClass()]
    public class MergeSortTests
    {
        [TestMethod()]
        public void SortTest()
        {
            int[] array = new int[] { 8, 5, 7, 9, 3, 2 };
            int[] expected = new int[] { 2, 3, 5, 7, 8, 9 };
            MergeSort ms = new MergeSort(array);
            DateTime before;
            DateTime after;
            before = DateTime.UtcNow;
            int[] actual = ms.Sort();
            Assert.IsTrue(helper_ArraysAreEqual(expected, actual));
            after = DateTime.UtcNow;
            TimeSpan runningtime = after - before;
        }

        [TestMethod()]
        public void SortTest_negative()
        {
            int[] array = new int[] { 8, 5, 7, 9, -3, -2 };
            int[] expected = new int[] { -2, -3, 5, 7, 8, 9 };
            MergeSort ms = new MergeSort(array);
            DateTime before;
            DateTime after;
            before = DateTime.UtcNow;
            int[] actual = ms.Sort();
            Assert.IsTrue(helper_ArraysAreEqual(expected, actual));
            after = DateTime.UtcNow;
            TimeSpan runningtime = after - before;
        }

        [TestMethod()]
        public void SortTest_oppoiste_sorted()
        {
            int[] array = new int[] { 9, 8, 7, 5, 3, 2};
            int[] expected = new int[] { 2, 3, 5, 7, 8, 9 };
            MergeSort ms = new MergeSort(array);
            DateTime before;
            DateTime after;
            before = DateTime.UtcNow;
            int[] actual = ms.Sort();
            Assert.IsTrue(helper_ArraysAreEqual(expected, actual));
            after = DateTime.UtcNow;
            TimeSpan runningtime = after - before;
        }

        [TestMethod()]
        public void SortTest_sorted()
        {
            int[] array = new int[] { 2, 3, 5, 7, 8, 9 };
            int[] expected = new int[] { 2, 3, 5, 7, 8, 9 };
            MergeSort ms = new MergeSort(array);
            DateTime before;
            DateTime after;
            before = DateTime.UtcNow;
            int[] actual = ms.Sort();
            Assert.IsTrue(helper_ArraysAreEqual(expected, actual));
            after = DateTime.UtcNow;
            TimeSpan runningtime = after - before;
        }

        private bool helper_ArraysAreEqual(IEnumerable<int> a, IEnumerable<int> b)
        {
            if (a == null && b == null)
            {
                return true;
            }
            else if (a == null || b == null)
            {
                return false;
            }
            else {
                if (a.Count() < b.Count())
                {
                    return false;
                }

                foreach (var v in a)
                {
                    if(!b.Contains(v))
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}