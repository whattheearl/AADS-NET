﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Hashtable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace Hashtable.Tests
{
    [TestClass()]
    public class TwoSumTests
    {
        [TestMethod()]
        public void TwoSumTest()
        {
            int[] a = new int[] { -3, 4, 3, 90 };
            int t = 0;

            TwoSumProblem search = new TwoSumProblem();
            var expected = new int[] { 0, 2 };
            var actual = search.TwoSum(a, t);

            if (!TestUtilities.ArraysAreEqual(expected, actual))
            {
                Assert.Fail(string.Format("Arrays aren't equal: expected[{0}], actual[{1}]", TestUtilities.EnumerableOfIntToString(expected), TestUtilities.EnumerableOfIntToString(actual)));
            }
        }

        [TestMethod()]
        public void TwoSumTest_1()
        {
            int[] a = new int[] { 3, 2, 4 };
            int t = 6;

            TwoSumProblem search = new TwoSumProblem();
            var expected = new int[] { 1, 2 };
            var actual = search.TwoSum(a, t);

            if (!TestUtilities.ArraysAreEqual(expected, actual))
            {
                Assert.Fail(string.Format("Arrays aren't equal: expected[{0}], actual[{1}]", TestUtilities.EnumerableOfIntToString(expected), TestUtilities.EnumerableOfIntToString(actual)));
            }
        }

        [TestMethod()]
        public void TwoSumTest_2()
        {
            int[] a = new int[] { 0, 3, 2, 4, 0 };
            int t = 0;

            TwoSumProblem search = new TwoSumProblem();
            var expected = new int[] { 0, 4 };
            var actual = search.TwoSum(a, t);

            if (!TestUtilities.ArraysAreEqual(expected, actual))
            {
                Assert.Fail(string.Format("Arrays aren't equal: expected[{0}], actual[{1}]", TestUtilities.EnumerableOfIntToString(expected), TestUtilities.EnumerableOfIntToString(actual)));
            }
        }

        /*[TestMethod()]
        public void TwoSumTest_3()
        {
            int[] a = TestUtilities.GetRandomArray(10000, -1000, 1000);
            int t = 0;

            BinarySearch search = new BinarySearch();
            var expected = search.TwoSum(a, t);
            var actual = search.TwoSum(a, t);

            if (!TestUtilities.ArraysAreEqual(expected, actual))
            {
                Assert.Fail(string.Format("Arrays aren't equal: expected[{0}], actual[{1}]", TestUtilities.EnumerableOfIntToString(expected), TestUtilities.EnumerableOfIntToString(actual)));
            }
        }*/
    }
}