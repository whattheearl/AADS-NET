﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtable
{
    /// <summary>
    /// http://yucoding.blogspot.com/2013/03/leetcode-question-113-two-sum.html
    /// </summary>
    public class TwoSumProblem
    {
        public int[] TwoSum(int[] nums, int target)
        {
            // create a hash table O(n)
            Dictionary<int, int> hmap = new Dictionary<int, int>();
            for (int i = 0; i < nums.Count(); i++)
            {
                hmap[nums[i]] = i;
            }

            // go through the elements in the array sequentially O(n)
            for (int i = 0; i < nums.Count(); i++)
            {
                // if the missing element (key) is in the hash table then use it's index (value) O(1)
                if (hmap.ContainsKey(target - nums[i]))
                {
                    int j = hmap[target - nums[i]];
                    if(i!=j) return new[] { i, j };
                }
            }

            return new int[0];
        }
    }
}
