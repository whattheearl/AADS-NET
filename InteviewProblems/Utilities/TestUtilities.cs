﻿using Graphs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public static class TestUtilities
    {
        public static bool ArraysAreEqual(IEnumerable<int> a, IEnumerable<int> b)
        {
            if (a == null && b == null)
            {
                return true;
            }
            else if (a == null || b == null)
            {
                return false;
            }
            else
            {
                if (a.Count() != b.Count())
                {
                    return false;
                }

                for (int i = 0; i < a.Count(); i++)
                {
                    if (a.ElementAtOrDefault(i) != b.ElementAtOrDefault(i))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public static bool ListNodeListsAreEqual(ListNode a, ListNode b)
        {
            if (a == null && b == null)
            {
                return true;
            }
            else
            {
                while(a!=null || b!=null)
                {
                    if((a==null && b!=null) || (a != null && b == null))
                    {
                        return false;
                    }

                    if(a.val != b.val)
                    {
                        return false;
                    }

                    a = a.next;
                    b = b.next;
                }
            }

            return true;
        }

        public static string EnumerableOfIntToString(IEnumerable<int> a)
        {
            string result = string.Empty;

            for (int i = 0; a != null && i < a.Count(); i++)
            {
                if (i > 0)
                {
                    result += ",";
                }
                result += a.ElementAtOrDefault(i);
            }

            return result;
        }

        public static int[] GetRandomArray(int size, int? min = null, int? max = null)
        {
            List<int> tmp = new List<int>();
            var random = new Random();
            for (int i = 0; i < size; i++)
            {
                tmp.Add(random.Next(min ?? 0, max ?? 100));
            }
            return tmp.ToArray();
        }
    }
}
